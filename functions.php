<?php

$GLOBALS['iva'] = 0.16;
$GLOBALS['tasa_win'] = 0.15;
$GLOBALS['pasarela'] = 0.0348;
$GLOBALS['administracion_efectivo'] = 0.09;
$GLOBALS['iva_secundario'] = 0.08;
$GLOBALS['retencion'] = 0.06;
$GLOBALS['otras_deducciones'] = 0.015;
$GLOBALS['seguro'] = 0.01;
$GLOBALS['gastos_administrativos'] = 0.02;
$GLOBALS['costo_token'] = 0.01484;

/* ------------ Configuracion -----------*/

function set_Iva() {
    //guardar en la base de datos
    return 0;
}

function set_Otras_Deducciones(){
    //guardar en la base de datos 
    return 0;
}

function set_Tiempo_Espera(){
    // revisar tiempos
    return 0;
}

/* ----------- End Configuracion ---------*/

/* --------------------------- Data base --------------- */

function get_Ganancias_Acumuladas_DB($dbConn, $UserID) {
    try {
        $query = $dbConn->prepare("SELECT rd.diario_ganancias FROM resumen_diario as rd
        WHERE rd.user_id = :user_id AND rd.created_at >= CURDATE() AND rd.created_at < CURDATE() + INTERVAL 1 DAY");
        $query->bindParam(":user_id", $UserID, PDO::PARAM_INT);
        $query->execute();
        $ganancias = $query->fetchAll(PDO::FETCH_ASSOC);
        //print_r($ganancias);
        if(count($ganancias) == 1){
            return $ganancias[0]['diario_ganancias'];
        }
    } catch (PDOException $ex) {
        print_r($ex);
    }
    return 0;
}

function get_Total_Usuario_Final_Efectivo($dbConn, $UserID) {
    //$tipo_pago = "efectivo";
    try {
        $query = $dbConn->prepare("SELECT SUM(mv.total_usuario_final) as total_usuario_final FROM resumen_diario AS rd
        LEFT JOIN movimientos as mv
        ON mv.resumen_id = rd.id AND mv.created_at >= CURDATE() AND mv.created_at < CURDATE() + INTERVAL 1 DAY AND mv.tipo_pago = 'efectivo'
        WHERE rd.user_id = :user_id");
        $query->bindParam(":user_id", $UserID, PDO::PARAM_INT);
        $query->execute();
        $total_usuario_final_efectivo = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($total_usuario_final_efectivo) > 0){
            if($total_usuario_final_efectivo[0]['total_usuario_final'] != null){
                return $total_usuario_final_efectivo[0]['total_usuario_final'];
            }
        }
    } catch (PDOException $ex) {
        print_r($ex);
    }
    return 0;
}
/* --------------------------- End Database ------------ */

/* ----------- Getters -------------------*/
function get_iva($tarifa_neta){
    return $tarifa_neta * $GLOBALS['iva'];
}

function get_Tiempo_de_espera() {
    return 0;
}

function get_Tasa_Win($tarifa_neta) {
    return $tarifa_neta * $GLOBALS['tasa_win'];
}

function get_Pasarela($tarifa_neta, $tipo_pago) {
    if ($tipo_pago == "credito"){
        return $tarifa_neta * $GLOBALS['pasarela'];
    }
    return 0;
}

function get_Administracion_Efectivo($tarifa_neta, $tipo_pago) {
    if ($tipo_pago == "efectivo"){
        return $tarifa_neta * $GLOBALS['administracion_efectivo'];
    }
    return 0;
    
}

function get_Retencion($tarifa_neta) { 
    return $tarifa_neta * $GLOBALS['retencion'];
}

function get_iva_Secundario($tarifa_neta){
    return $tarifa_neta * $GLOBALS['iva_secundario'];
}

function get_Otras_Deducciones($tarifa_neta, $otras_deducciones) {
    //echo $otras_deducciones;
    return $tarifa_neta * $otras_deducciones;
}

function get_Seguro($tarifa_neta) {
    return $tarifa_neta * $GLOBALS['seguro'];
}

function get_Gastos_Administrativos($tarifa_neta) {
    return $tarifa_neta * $GLOBALS['gastos_administrativos'];
}

function get_Costo_Tokenizacion($tarifa_neta, $tipo_pago){
    if ($tipo_pago == "credito"){
        return $tarifa_neta * $GLOBALS['costo_token'];
    }
    return 0;
}
/* -------------- End Getters ----------*/

/* -------------- Totales --------------*/
function get_Sum_Impuestos($tarifa_neta, $tipo_pago, $otras_deducciones){
    return get_Tasa_Win($tarifa_neta) + get_Pasarela($tarifa_neta, $tipo_pago) + get_Administracion_Efectivo($tarifa_neta, $tipo_pago) + get_iva_Secundario($tarifa_neta) + get_Otras_Deducciones($tarifa_neta, $otras_deducciones) + get_Seguro($tarifa_neta) + get_Retencion($tarifa_neta) + get_Gastos_Administrativos($tarifa_neta) + get_Costo_Tokenizacion($tarifa_neta, $tipo_pago);
}

function get_Total_Usuario_Final($tarifa_neta) {
    return $tarifa_neta + get_iva($tarifa_neta);
}

function get_Ganancias($tarifa_neta, $tipo_pago, $otras_deducciones) {
    return get_Total_Usuario_Final($tarifa_neta) - get_sum_Impuestos($tarifa_neta, $tipo_pago, $otras_deducciones); 
}

function get_Ganancias_Acumuladas($dbConn, $tarifa_neta, $tipo_pago, $otras_deducciones, $UserID) {
    $ganancias = get_Ganancias_Acumuladas_DB($dbConn, $UserID);
    return $ganancias + get_Ganancias($tarifa_neta, $tipo_pago, $otras_deducciones);
}

function get_Saldo_Operativo($dbConn, $UserID, $tarifa_neta, $tipo_pago, $otras_deducciones) {
    $total_usuario_final_efectivo = get_Total_Usuario_Final_Efectivo($dbConn, $UserID);
    if($tipo_pago == "efectivo"){
        $total_usuario_final_efectivo += get_Total_Usuario_Final($tarifa_neta);
    }
    return get_Ganancias_Acumuladas($dbConn, $tarifa_neta, $tipo_pago, $otras_deducciones, $UserID) - $total_usuario_final_efectivo;
}
/* ------------- End Totales -------------*/

/* ------------- Parse Json --------------*/
function get_Parse_Json($bindable_objects){
    return 0;
}

?>