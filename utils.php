<?php
include "functions.php";

function connect($db) {
    try {
        $conn = new PDO("mysql:host={$db['host']};dbname={$db['db']};charset=utf8", $db['username'], $db['password']);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        return $conn;
    } catch (PDOException $exception) {
        exit($exception->getMessage());
    }
}

// --------------------------  functions to make token
function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

function generateToken(){
    return hash('sha256', generateRandomString());
}
// --------------------------- end functions to make token

// --------------------------- insert tokens
function insertAuthorizedApp($dbConn, $AppToken){
    try {
        $query = $dbConn->prepare("INSERT INTO authorized_apps (token) VALUES (:token)");
        $query->bindParam(":token", $AppToken, PDO::PARAM_STR);
        $query->execute();
    } catch (PDOException $ex) {
        //print_r($ex);
        return false;
    }
    return true;
}

function insertAuthorizedUser($dbConn, $Authorized_App_Id, $UserToken){
    try {
        $query = $dbConn->prepare("INSERT INTO users (token, authorized_app_id) VALUES (:token, :authorized_app_id)");
        $query->bindParam(":token", $UserToken, PDO::PARAM_STR);
        $query->bindParam(":authorized_app_id", $Authorized_App_Id, PDO::PARAM_INT);
        $query->execute();
    } catch (PDOException $ex) {
        //print_r($ex);
        return false;
    }
    return true;
}
// ------------------------------ end insert tokens 

// ------------------------------ check tokens
function checkAppToken($dbConn,$AppToken) { // devuelve el objeto o null
    try {
        $query = $dbConn->prepare("SELECT * FROM authorized_apps WHERE token = :app_token");
        $query->bindParam(":app_token", $AppToken, PDO::PARAM_STR);
        $query->execute();
        $authorized_apps = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($authorized_apps) == 1){
            if($authorized_apps[0]['id'] != null ){
                return $authorized_apps[0];
            }
        }
    } catch (PDOException $ex) {
        //print_r($ex);
    }
    return null;
}

function checkUserToken($dbConn, $UserToken, $AppToken) { // devuelve el objeto o null
    try {
        $query = $dbConn->prepare("SELECT * FROM authorized_apps as autaps
        LEFT JOIN users as usr
        ON usr.token = :user_token AND usr.authorized_app_id = autaps.id
        WHERE autaps.token = :app_token");
        $query->bindParam(":user_token", $UserToken, PDO::PARAM_STR);
        $query->bindParam(":app_token", $AppToken, PDO::PARAM_STR);
        $query->execute();
        $users = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($users) == 1){
            if($users[0]['id'] != null){
                return $users[0];
            }
        }
    } catch (PDOException $ex) {
        //print_r($ex);
    }
    return null;
}

function checkOtrasDeducciones($dbConn, $deducciones){
    try {
        $query = $dbConn->prepare("SELECT * FROM otras_deducciones WHERE name = :name");
        $query->bindParam(":name", $deducciones, PDO::PARAM_STR);
        $query->execute();
        $tarifa_otras_deducciones = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($tarifa_otras_deducciones) == 1){
            return $tarifa_otras_deducciones[0]['tarifa'];
            //return $tarifa_neta * 
        }
    } catch (PDOException $ex) {
        //print_r($ex);
    }
    return null;
}
// ------------------------------ end check tokens

// ------------------------------ Dates functions
function getDateFromMovement($dbConn, $LastMovementID){
    try {
        $query = $dbConn->prepare("SELECT * FROM movimientos WHERE id = :id_movimiento");
        $query->bindParam(":id_movimiento", $LastMovementID, PDO::PARAM_STR);
        $query->execute();
        $movements = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($movements) == 1){
            return $movements[0]['created_at'];
        }
    } catch (PDOException $ex) {
        // print_r($ex);
    }
    return null;
}

function getResumenAssoc($dbConn, $date, $UserID) {
    try {
        $query = $dbConn->prepare("SELECT id, created_at FROM resumen_diario WHERE user_id = :user_id");
        $query->bindParam(":user_id", $UserID, PDO::PARAM_STR);
        $query->execute();
        $resumenes = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($resumenes) > 0){
            foreach($resumenes as $resumen) {
                if ($date == getFormatDate($resumen['created_at'])){
                    return $resumen;
                }
            }
        }
    } catch (PDOException $ex) {
        // print_r($ex);
    }
    return null;
}

function getFormatDate($date) {
    if($date != null ){
        $date = new DateTime($date);
        return $date->format('Y-m-d');
    }
    return null;
}
// ------------------------------ end dates function

// ------------------------------ DB functions
function insertNewMovement($dbConn, $ObjData, $UserID) {
    $tipo_pago = $ObjData->tipo_pago;
    $tarifa_neta = $ObjData->tarifa_neta;
    $iva = $ObjData->iva;
    $total_usuario_final = $ObjData->total_usuario;
    $tiempo_espera = $ObjData->tiempo_espera;
    $tasa_win = $ObjData->tasa_win;
    $pasarela = $ObjData->pasarela;
    $administracion_efectivo = $ObjData->administracion_efectivo;
    $iva_secundario = $ObjData->iva_secundario;
    $retencion = $ObjData->retencion;
    $otras_deducciones = $ObjData->otras_deducciones;
    $seguro = $ObjData->seguro;
    $gastos_administrativos = $ObjData->gastos_administrativos;
    $ganancias = $ObjData->ganancias;
    $ganancias_acumuladas = $ObjData->ganancias_acumuladas;
    $saldo_operativo = $ObjData->saldo_operativo;
    $costo_token = $ObjData->costo_token;
    try {
        $query = $dbConn->prepare("INSERT INTO movimientos (user_id, tarifa_neta, tipo_pago, iva, total_usuario_final, tiempo_espera, tasa_win, pasarela, administracion_efectivo, iva_secundario, retencion, otras_deducciones, seguro, gastos_administrativos, ganancias, ganancias_acumuladas, saldo_operativo, costo_token) 
        VALUES (:user_id, :tarifa_neta, :tipo_pago, :iva, :total_usuario_final, :tiempo_espera, :tasa_win, :pasarela, :administracion_efectivo, :iva_secundario, :retencion, :otras_deducciones, :seguro, :gastos_administrativos, :ganancias, :ganancias_acumuladas, :saldo_operativo, :costo_token)");
        $query->bindParam(":user_id", $UserID , PDO::PARAM_INT);
        $query->bindParam(":tipo_pago", $tipo_pago, PDO::PARAM_STR);
        $query->bindParam(":tarifa_neta", $tarifa_neta, PDO::PARAM_STR);
        $query->bindParam(":iva", $iva, PDO::PARAM_STR );
        $query->bindParam(":total_usuario_final", $total_usuario_final, PDO::PARAM_STR );
        $query->bindParam(":tiempo_espera", $tiempo_espera, PDO::PARAM_STR );
        $query->bindParam(":tasa_win", $tasa_win , PDO::PARAM_STR );
        $query->bindParam(":pasarela", $pasarela , PDO::PARAM_STR );
        $query->bindParam(":administracion_efectivo", $administracion_efectivo , PDO::PARAM_STR );
        $query->bindParam(":iva_secundario", $iva_secundario , PDO::PARAM_STR );
        $query->bindParam(":retencion", $retencion , PDO::PARAM_STR );
        $query->bindParam(":otras_deducciones", $otras_deducciones , PDO::PARAM_STR );
        $query->bindParam(":seguro", $seguro , PDO::PARAM_STR );
        $query->bindParam(":gastos_administrativos", $gastos_administrativos , PDO::PARAM_STR );
        $query->bindParam(":ganancias", $ganancias , PDO::PARAM_STR );
        $query->bindParam(":ganancias_acumuladas",  $ganancias_acumuladas , PDO::PARAM_STR );
        $query->bindParam(":saldo_operativo", $saldo_operativo , PDO::PARAM_STR );
        $query->bindParam(":costo_token", $costo_token , PDO::PARAM_STR );
        $query->execute();
        return $dbConn->lastInsertId();
    } catch (PDOException $ex) {
        print_r($ex);
    }
    return null;
}

function insertNewResumen($dbConn, $ObjData, $UserID) {
    $diario_tiempo_espera = 0;
    $diario_tarifa_neta = $ObjData->tarifa_neta;
    $diario_iva = $ObjData->iva;
    $diario_total_usuario_final = $ObjData->total_usuario;
    $diario_tiempo_espera = $ObjData->tiempo_espera;
    $diario_tasa_win = $ObjData->tasa_win;
    $diario_pasarela = $ObjData->pasarela;
    $diario_administracion_efectivo = $ObjData->administracion_efectivo;
    $diario_iva_secundario = $ObjData->iva_secundario;
    $diario_retencion = $ObjData->retencion;
    $diario_otras_deducciones = $ObjData->otras_deducciones;
    $diario_seguro = $ObjData->seguro;
    $diario_gastos_administrativos = $ObjData->gastos_administrativos;
    $diario_ganancias = $ObjData->ganancias;
    $diario_costo_token = $ObjData->costo_token;
    $diario_saldo_operativo = getResumenOperativeBalance($dbConn, 0 , $ObjData->user_id) + $ObjData->saldo_operativo;
    try {
        $query = $dbConn->prepare("INSERT INTO resumen_diario (user_id, diario_tarifa_neta, diario_iva, diario_total_usuario_final ,diario_tiempo_espera, diario_tasa_win, diario_pasarela, diario_administracion_efectivo, diario_iva_secundario, diario_retencion, diario_otras_deducciones, diario_seguro, diario_gastos_administrativos, diario_ganancias, diario_saldo_operativo, diario_costo_token) 
        VALUES (:user_id, :diario_tarifa_neta, :diario_iva, :diario_total_usuario_final, :diario_tiempo_espera, :diario_tasa_win, :diario_pasarela, :diario_administracion_efectivo, :diario_iva_secundario, :diario_retencion, :diario_otras_deducciones, :diario_seguro, :diario_gastos_administrativos, :diario_ganancias, :diario_saldo_operativo, :diario_costo_token)");
        $query->bindParam(":user_id", $UserID , PDO::PARAM_INT);
        $query->bindParam(":diario_tarifa_neta", $diario_tarifa_neta, PDO::PARAM_STR);
        $query->bindParam(":diario_iva", $diario_iva, PDO::PARAM_STR );
        $query->bindParam(":diario_total_usuario_final", $diario_total_usuario_final, PDO::PARAM_STR);
        $query->bindParam(":diario_tiempo_espera", $diario_tiempo_espera, PDO::PARAM_STR);
        $query->bindParam(":diario_tasa_win", $diario_tasa_win , PDO::PARAM_STR);
        $query->bindParam(":diario_pasarela", $diario_pasarela , PDO::PARAM_STR);
        $query->bindParam(":diario_administracion_efectivo", $diario_administracion_efectivo , PDO::PARAM_STR);
        $query->bindParam(":diario_iva_secundario", $diario_iva_secundario , PDO::PARAM_STR);
        $query->bindParam(":diario_retencion", $diario_retencion , PDO::PARAM_STR);
        $query->bindParam(":diario_otras_deducciones", $diario_otras_deducciones , PDO::PARAM_STR);
        $query->bindParam(":diario_seguro", $diario_seguro , PDO::PARAM_STR);
        $query->bindParam(":diario_gastos_administrativos", $diario_gastos_administrativos , PDO::PARAM_STR);
        $query->bindParam(":diario_ganancias", $diario_ganancias , PDO::PARAM_STR);
        $query->bindParam(":diario_saldo_operativo", $diario_saldo_operativo , PDO::PARAM_STR);
        $query->bindParam(":diario_costo_token", $diario_costo_token , PDO::PARAM_STR);
        $query->execute();
        return $dbConn->lastInsertId();
    } catch (PDOException $ex) {
        print_r($ex);
    }
    return null;
}

function connectMovementsToResumen($dbConn, $MovementID, $ResumenID){
    try {
        $query = $dbConn->prepare("UPDATE movimientos SET resumen_id=:resumen_id WHERE id=:movement_id");
        $query->bindParam(":movement_id", $MovementID, PDO::PARAM_INT);
        $query->bindParam(":resumen_id", $ResumenID, PDO::PARAM_INT);
        $query->execute();
        return true;
    }catch (PDOException $ex) {
        // print_r($ex);
    }
    return false;
}

function getOperativeBalance($dbConn, $UserID) {
    try {
        $query = $dbConn->prepare("SELECT SUM(rd.diario_saldo_operativo) 
        FROM resumen_diario as rd
        WHERE rd.user_id = user_id");
        $query->bindParam(":user_id", $UserID, PDO::PARAM_INT);
        $query->execute();
        $saldo_operativo_total = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($saldo_operativo_total) == 1 && $saldo_operativo_total != null){
            return $saldo_operativo_total['diario_saldo_operativo'];
        }
    }catch (PDOException $ex) {
        // print_r($ex);
    }
    return 0;
}

function getAcumMovements($dbConn, $ResumenID ) {
    try {
        $query = $dbConn->prepare("SELECT SUM(mv.tarifa_neta) as tarifa_neta,
        SUM(mv.iva) as iva,
        SUM(mv.total_usuario_final) as total_usuario_final,
        SUM(mv.tiempo_espera) as tiempo_espera,
        SUM(mv.tasa_win) as tasa_win,
        SUM(mv.pasarela) as pasarela,
        SUM(mv.administracion_efectivo) as administracion_efectivo,
        SUM(mv.iva_secundario) as iva_secundario,
        SUM(mv.retencion) as retencion,
        SUM(mv.otras_deducciones) as otras_deducciones,
        SUM(mv.seguro) as seguro,
        SUM(mv.gastos_administrativos) as gastos_administrativos,
        SUM(mv.ganancias) as ganancias,
        SUM(mv.ganancias_acumuladas) as ganancias_acumuladas,
        SUM(mv.saldo_operativo) as saldo_operativo,
        SUM(mv.costo_token) as costo_token
        FROM movimientos as mv
        LEFT JOIN resumen_diario as rd
        ON mv.resumen_id = rd.id
        WHERE rd.id = :resumen_id");
        $query->bindParam(":resumen_id", $ResumenID, PDO::PARAM_INT);
        $query->execute();
        $movimientos = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($movimientos) == 1 && $movimientos != null){
            return $movimientos[0];
        }
    }catch (PDOException $ex) {
        // print_r($ex);
    }
    return null;
}

function updateResumenFromMovement($dbConn, $ResumenID, $AcumResult, $ObjData) {
    //$saldo_operativo = 
    $tarifa_neta = $AcumResult['tarifa_neta'];
    $iva = $AcumResult['iva'];
    $total_usuario_final = $AcumResult['total_usuario_final'];
    $tiempo_espera = $AcumResult['tiempo_espera'];
    $tasa_win = $AcumResult['tasa_win'];
    $pasarela = $AcumResult['pasarela'];
    $administracion_efectivo = $AcumResult['administracion_efectivo'];
    $iva_secundario = $AcumResult['iva_secundario'];
    $retencion = $AcumResult['retencion'];
    $otras_deducciones = $AcumResult['otras_deducciones'];
    $seguro = $AcumResult['seguro'];
    $gastos_administrativos = $AcumResult['gastos_administrativos'];
    $costo_token = $AcumResult['costo_token'];
    //$ganancias_acumuladas = $ObjData->ganancias_acumuladas;
    $saldo_operativo = getResumenOperativeBalance($dbConn, $ResumenID, $ObjData->user_id) + $ObjData->saldo_operativo;
    $ganancias = $ObjData->ganancias_acumuladas;//$AcumResult['ganancias'];
    try {
        $query = $dbConn->prepare("UPDATE resumen_diario 
        SET
        diario_tarifa_neta = :diario_tarifa_neta,
        diario_iva = :diario_iva,
        diario_total_usuario_final = :diario_total_usuario_final,
        diario_tiempo_espera = :diario_tiempo_espera,
        diario_tasa_win = :diario_tasa_win,
        diario_pasarela = :diario_pasarela,
        diario_administracion_efectivo = :diario_administracion_efectivo,
        diario_iva_secundario = :diario_iva_secundario,
        diario_retencion = :diario_retencion,
        diario_otras_deducciones = :diario_otras_deducciones,
        diario_seguro = :diario_seguro,
        diario_gastos_administrativos = :diario_gastos_administrativos,
        diario_ganancias = :diario_ganancias,
        diario_saldo_operativo = :diario_saldo_operativo,
        diario_costo_token = :diario_costo_token
        WHERE id=:resumen_id");
        $query->bindParam(":diario_tarifa_neta", $tarifa_neta , PDO::PARAM_STR);
        $query->bindParam(":diario_iva", $iva, PDO::PARAM_STR );
        $query->bindParam(":diario_total_usuario_final", $total_usuario_final, PDO::PARAM_STR);
        $query->bindParam(":diario_tiempo_espera", $tiempo_espera , PDO::PARAM_STR);
        $query->bindParam(":diario_tasa_win", $tasa_win , PDO::PARAM_STR);
        $query->bindParam(":diario_pasarela", $pasarela , PDO::PARAM_STR);
        $query->bindParam(":diario_administracion_efectivo", $administracion_efectivo , PDO::PARAM_STR);
        $query->bindParam(":diario_iva_secundario", $iva_secundario , PDO::PARAM_STR);
        $query->bindParam(":diario_retencion", $retencion  , PDO::PARAM_STR);
        $query->bindParam(":diario_otras_deducciones", $otras_deducciones , PDO::PARAM_STR);
        $query->bindParam(":diario_seguro", $seguro , PDO::PARAM_STR);
        $query->bindParam(":diario_gastos_administrativos", $gastos_administrativos , PDO::PARAM_STR);
        $query->bindParam(":diario_ganancias", $ganancias , PDO::PARAM_STR);
        $query->bindParam(":diario_saldo_operativo", $saldo_operativo, PDO::PARAM_STR);
        $query->bindParam(":diario_costo_token", $costo_token, PDO::PARAM_STR);
        $query->bindParam(":resumen_id", $ResumenID, PDO::PARAM_INT);
        $query->execute();
        return true;
    }catch (PDOException $ex) {
        //print_r($ex);
    }
    return false;
    
}

function getResumenOperativeBalance($dbConn, $ResumenID, $UserID){ // ni idea de si rompi algo
    try {
        $query = $dbConn->prepare("SELECT SUM(rd.diario_saldo_operativo) AS diario_saldo_operativo 
        FROM resumen_diario as rd 
        WHERE rd.user_id = :user_id && rd.id != :resumen_id");
        $query->bindParam(":user_id", $UserID, PDO::PARAM_INT);
        $query->bindParam(":resumen_id", $ResumenID, PDO::PARAM_INT);
        $query->execute();
        $saldo_operativo_total = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($saldo_operativo_total) == 1 && $saldo_operativo_total != null){
            return $saldo_operativo_total[0]['diario_saldo_operativo'];
        }
    }catch (PDOException $ex) {
        // print_r($ex);
    }
    return 0;
}

function getResumenUsersData($dbConn){
    try {
        $query = $dbConn->prepare("SELECT rd1.* 
        FROM resumen_diario AS rd1
        INNER JOIN
        (
            SELECT max(created_at) AS lastdate, user_id
            FROM resumen_diario
            GROUP BY user_id
        ) AS rd2
        ON rd1.user_id = rd2.user_id AND rd1.created_at = rd2.lastdate
        ORDER BY rd1.created_at DESC ");
        $query->execute();
        $resumenes = $query->fetchAll(PDO::FETCH_ASSOC);
        //echo count($resumenes);

        if($resumenes != null && count($resumenes) >= 1){
            return $resumenes;
        }
    }catch (PDOException $ex) {
        //print_r($ex);
    }
    return null;
}

// ------------------------------ END DB functions

// ------------------------------ primary functions
function getAppToken($dbConn){
    $AppToken = generateToken();
    if(insertAuthorizedApp($dbConn, $AppToken) == true){
        return $AppToken;
    }
    return null;
}

function getUserToken($dbConn, $AppToken){
    $UserToken = generateToken();
    $Authorized_App = checkAppToken($dbConn, $AppToken);
    if($Authorized_App != null){
        if (insertAuthorizedUser($dbConn, $Authorized_App['id'], $UserToken) == true){
            return $UserToken;
        }
    }
    return null;
}

function SetMovement($dbConn, $AppToken, $UserToken, $ObjData, $UserID){
    $errors = array();
    $LastMovementID = insertNewMovement($dbConn, $ObjData, $UserID); // obtiene el id despues de insertar el movimiento
    //print_r($LastMovementID);
    if($LastMovementID != null) { // revisa que el movimiento exista y se haya insertado correctamente
        array_push($errors, ["status" => "success", "info" => "Movimiento agregado correctamente"]);
        $dateFromMovement = getFormatDate(getDateFromMovement($dbConn, $LastMovementID)); // obtiene la fecha obtenida ya formateada
        if ($dateFromMovement != null){ // revisa que la fecha del movimiento exista
            array_push($errors, ["status" => "success", "info" => "Formato de fecha correcto"]);
            $Resumen = getResumenAssoc($dbConn, $dateFromMovement, $UserID); // revisa que exista un resumen creado con la misma fecha y obtiene el id y la fecha del resumen
            //Esto debe cambiar a revisar que exista un resumen con la misma fecha y que no haya sido pagado 
            if ($Resumen != null){ // si existe un resumen
                array_push($errors, ["status" => "success", "info" => "Resumen encontrado"]);
                if(connectMovementsToResumen($dbConn, $LastMovementID, $Resumen['id'] )){ // se ligan correctamente
                    array_push($errors, ["status" => "success", "info" => "Movimiento relacionado al resumen correctamente"]);
                    if(updateResumenFromMovement($dbConn, $Resumen['id'], getAcumMovements($dbConn, $Resumen['id']), $ObjData)) {
                        array_push($errors, ["status" => "success", "info" => "Datos del Resumen actualizados correctamente"]);
                    } else {
                        array_push($errors, ["status" => "failed", "info" => "No se ha podido actualizar el Resumen"]);
                    }
                } else {
                    array_push($errors, ["status" => "failed", "info" => "Error al relacionar al Resumen y su Movimiento"]);
                }
            }else { // si no se creara un nuevo resumen para despues ligar el movimiento a el y se actualizaran
                $LastResumenID = insertNewResumen($dbConn, $ObjData, $UserID);
                if($LastResumenID != null){
                    array_push($errors, ["status" => "success", "info" => "Resumen agregado correctamente"]);
                    if(connectMovementsToResumen($dbConn, $LastMovementID, $LastResumenID)) {
                        array_push($errors, ["status" => "success", "info" => "Movimiento relacionado al Resumen correctamente"]);
                    }else {
                        array_push($errors, ["status" => "failed", "info" => "Error al relacionar Resumen y su Movimiento"]);
                    }
                }else {
                    array_push($errors, ["status" => "failed", "info" => "No se pudo agregar un Resumen correctamente"]);
                }
            }
        } else {
            array_push($errors, ["status" => "failed", "info" => "Formato de fecha incorrecto"]);
        }
    } else {
        array_push($errors, ["status" => "failed", "info" => "Moviento no ha sido agregado correctamente"]);
    }
    return $errors;
}

function getParamsFromsArray($dbConn, $array, $UserID){
    $NewMovement = new stdClass();
    $NewMovement->Result = null;
    $NewMovement->errors = array();
    if( isset($array)){
        if( $array->type == "efectivo" || $array->type == "credito"  ) {
            if( is_numeric($array->tarifa_neta)) {
                $deducciones_tarifa = checkOtrasDeducciones($dbConn, $array->deducciones);
                if ($deducciones_tarifa != null ){
                    $Result = new stdClass();
                    $Result->user_id = $UserID;
                    $Result->tarifa_neta = $array->tarifa_neta;
                    $Result->iva = get_iva($array->tarifa_neta);
                    $Result->tipo_pago = $array->type;
                    $Result->total_usuario = get_Total_Usuario_Final($array->tarifa_neta);
                    $Result->tiempo_espera = 0; // pendiente
                    $Result->tasa_win = get_Tasa_Win($array->tarifa_neta);
                    $Result->pasarela = get_Pasarela($array->tarifa_neta, $array->type);
                    $Result->administracion_efectivo = get_Administracion_Efectivo($array->tarifa_neta, $array->type);
                    $Result->iva_secundario = get_iva_Secundario($array->tarifa_neta);
                    $Result->retencion = get_Retencion($array->tarifa_neta);
                    $Result->otras_deducciones = get_Otras_Deducciones($array->tarifa_neta, $deducciones_tarifa); // Cambiar esta funcion por la nueva
                    $Result->seguro = get_Seguro($array->tarifa_neta);
                    $Result->gastos_administrativos = get_Gastos_Administrativos($array->tarifa_neta);
                    $Result->ganancias_acumuladas = get_Ganancias_Acumuladas($dbConn, $array->tarifa_neta, $array->type, $deducciones_tarifa, $UserID);
                    $Result->saldo_operativo = get_Saldo_Operativo($dbConn, $UserID, $array->tarifa_neta, $array->type, $deducciones_tarifa);
                    $Result->ganancias = get_Ganancias($array->tarifa_neta, $array->type, $deducciones_tarifa);
                    $Result->costo_token = get_Costo_Tokenizacion($array->tarifa_neta, $array->type);
                    $NewMovement->Result = $Result;
                    array_push($NewMovement->errors, ["status" => "success", "info" => "Datos Obtenidos Correctamente"]);
                }else {
                    array_push($NewMovement->errors, ["status" => "failed", "info" => "Error: Código de deducciones desconocido"]);
                }
            }else {
                array_push($NewMovement->errors, ["status" => "failed", "info" => "Error: Tarifa neta no es un tipo de dato válido"]);
            }
        } else {
            array_push($NewMovement->errors, ["status" => "failed", "info" => "Error: Tipo de pago desconocido"]);
        }
    } else {
        array_push($NewMovement->errors, ["status" => "failed", "info" => "Error: Incongruencia de datos"]);
    }
    return $NewMovement;
}

// ------------------------------- Obtener parametros para updates
function getParams($input) {
    $filterParams = [];
    foreach($input as $param => $value)
    {
            $filterParams[] = "$param=:$param";
    }
    return implode(", ", $filterParams);
}

// ------------------------------- Asociar todos los parametros a un sql
function bindAllValues($statement, $params) {
    foreach($params as $param => $value)
    {
        $statement->bindValue(':'.$param, $value);
    }
    return $statement;
}
?>