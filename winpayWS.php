<?php
header('Content-Type: application/json; charset=UTF-8');
include "config.php";
include "utils.php";

$dbConn = connect($db);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    //echo "Entra GET Method";
    $input = $_GET;
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $data = json_decode($json);

    $response = new stdClass();
    $response->errors = array();
    if(isset($data)){
        if(isset($data->getData) && isset($data->AdminToken)){
            if($data->getData == "Resumen" && $data->AdminToken =="Hash"){
                $response->result = "";
                $response->result = getResumenUsersData($dbConn);
            }else {
                array_push($response->errors, ["status" => "failed", "info" => "Seguridad: Credenciales no consistentes"]);
                //print_r("No entra");
            }
        }else{
            array_push($response->errors, ["status" => "failed", "info" => "Error: Faltan algunas directivas (Solicitud rechazada)"]);
            //print_r("data doesnt exist");
        }
    }else{
        array_push($response->errors, ["status" => "failed", "info" => "Error: Formato de petición incorrecto"]);
    }
    //header("HTTP/1.1 200 OK");
    if(isset($response->result)){
        echo json_encode($response->result, JSON_UNESCAPED_UNICODE);
        //print_r($response->result);
    }else {
        echo json_encode($response->errors, JSON_UNESCAPED_UNICODE);
        //json_encode($response->errors);
    }
    exit();
}


// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // get Parameters from request
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $data = json_decode($json);
    $response = new stdClass();
    $response->errors = array();

    if($data != null){
        if(isset($data)){
            if(isset($data->getToken)){
                $response->AppToken = "";
                $response->UserToken = "";
                if($data->getToken == "App"){
                    array_push($response->errors, ["status" => "success", "info" => "Solicitud: AppToken"]);
                    $AppToken = getAppToken($dbConn);
                    if($AppToken != null ) {
                        $response->AppToken = $AppToken;
                    }else {
                        $response->AppToken = "";
                        array_push($response->errors, ["status" => "failed", "info" => "Error: AppToken no obtenido"]);
                    }
                }else if($data->getToken == "User") {
                    if(isset($data->AppToken)){
                        array_push($response->errors, ["status" => "success", "info" => "Solicitud: UserToken"]);
                        $UserToken = getUserToken($dbConn, $data->AppToken);
                        if($UserToken != null){
                            $response->UserToken = $UserToken;
                        }else {
                            $response->UserToken = "";
                            array_push($response->errors, ["status" => "failed", "info" => "Error: AppToken no válido"]);
                        }
                    }else {
                        array_push($response->errors, ["status" => "failed", "info" => "Error: Falta directiva (AppToken)"]);
                    }
                } else {
                    array_push($response->errors, ["status" => "failed", "info" => "Error: Solicitud no válida"]);
                }
            }else {
                if(isset($data->AppToken) && isset($data->UserToken) && isset($data->tarifa_neta) && isset($data->deducciones) && isset($data->type)){
                    array_push($response->errors, ["status" => "success", "info" => "Solicitud: Agregar movimiento"]);
                    $AppToken = $data->AppToken;
                    $UserToken = $data->UserToken;
                    if (checkAppToken($dbConn, $AppToken) != null) { //Buscar el Token en la base de datos
                        $user = checkUserToken($dbConn, $UserToken, $AppToken); //Revisa que el Token de usuario exista y perteneza a una aplicacion
                        //print_r($user);
                        if ($user != null) { // revisa que el usuario exista y pertenezca al token de seguridad
                            $ObjData = getParamsFromsArray($dbConn, $data, $user['id']);
                            array_push($response->errors, $ObjData->errors);
                            if($ObjData->Result != null) {
                                $response_movement = SetMovement($dbConn, $AppToken, $UserToken, $ObjData->Result, $user['id']);
                                array_push($response->errors, $response_movement);
                                if(end($response->errors)[0]['status'] != "failed"){
                                    array_push($response->errors, ["status" => "success", "info" => "Movimiento agregado correctamente"]);
                                    $response->movimiento = new stdClass();
                                    $response->movimiento = $ObjData->Result;
                                }else {
                                    array_push($response->errors, ["status" => "success", "info" => "Seguridad: Credenciales no consistentes"]);
                                }
                            } else {
                                array_push($response->errors, ["status" => "failed", "info" => "Movimiento no agregado"]);
                            }
                        } else {
                            array_push($response->errors, ["status" => "failed", "info" => "UserToken no válido"]);
                            
                        }
                    } else {
                        array_push($response->errors, ["status" => "failed", "info" => "AppToken no válido"]);                   
                    }
                }else {
                    array_push($response->errors, ["status" => "failed", "info" => "Error: Faltan algunas directivas (Solicitud rechazada)"]);
                }
            }
        }
    }else{
        array_push($response->errors, ["status" => "failed", "info" => "Error: Formato de petición incorrecto"]);
    }
    if(isset($response->movimiento)){
        //echo json_encode($response->movimiento);
        print_r($response->movimiento, JSON_UNESCAPED_UNICODE);
    }else {
        print_r($response, JSON_UNESCAPED_UNICODE);
        //echo json_encode($response);
    }

    exit();
}

//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    //echo "Entra DELETE Method";
    $input = $_GET;
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    //$data = json_decode($json);
    exit();
}

//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    //echo "Entra PUT Method";
    $input = $_GET;
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    //$data = json_decode($json);
    exit();
} 
?>